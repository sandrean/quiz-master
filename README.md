# Quiz Master

## Description

A simple delivery game created while learning the C# Unity Game Developer 2D course by gamedev.tv. The intended purpose of this game was to teach me how to use TextMeshPro, buttons, sliders, and scriptable objects. This quiz contains 5 questions that are presented in a random order that have answers at set indices. There is an indicator to show what percentage of questions the player gets right.

### Dependencies

- Unity v2020.3.22f1

### Executing program

Create a new project on unity. Add all the following files within the base folder of the unity project. Go to build settings using ctrl + shift + b to build the project to the desired settings in a new folder to generate the executable for the game.

